(function ($) {
	"use strict";
    jQuery(document).ready(function($){
       /**-----------------------------
        *   extended price popup
        * ---------------------------**/
       $(document).on('click','.regular_price',function(){
            $(this).next().toggleClass('active');
       });
        /*-------------------------------------
        *   Marginific popup for screenshort
        * ------------------------------------*/
       $('.mfscreent').magnificPopup({
           type:'image',
           gallery: {
            enabled: true
          },
       });
        $(document).on('click','.open_screenshort',function(e){
            e.preventDefault();
           $('.mfscreent').magnificPopup('open');
        });
        /**--------------------------------
         *  Google Recaptcha Responsive
         * ------------------------------**/
        var winwidth = $(window).width();

        if(winwidth > 768 && winwidth <= 991 ){
            $('.g-recaptcha').css({
                transform:'scale(2.090)',
                clear : 'both',
                'margin-bottom': '100px'
            });

        } else if(winwidth > 599 && winwidth < 768){
            $('.g-recaptcha').css({
                transform:'scale(1.49)',
                clear : 'both',
                'margin-bottom': '50px'
            });
        } else if(winwidth > 451 && winwidth < 599){
            $('.g-recaptcha').css({
                transform:'scale(1.22)',
                clear : 'both',
                'margin-bottom': '20px'
            });
        } else if(winwidth > 415 && winwidth < 450){
            $('.g-recaptcha').css({
                transform:'scale(1.09)',
                clear : 'both',
                'margin-bottom': '20px'
            });
        }
        else if(winwidth > 384 && winwidth < 414){
            $('.g-recaptcha').css({
                transform:'scale(1)',
                clear : 'both',
                'margin-bottom': '0px'
            });
        }
        else if(winwidth > 360 && winwidth < 384){
            $('.g-recaptcha').css({
                transform:'scale(0.9)',
                clear : 'both',
                'margin-bottom': '0px'
            });
        }
        else if(winwidth >= 320 && winwidth < 360){
            $('.g-recaptcha').css({
                transform:'scale(.76)',
                clear : 'both',
                'margin-bottom': '0px'
            });
        }
        /**--------------------------------
         *  Forum Page Search Module
         * ------------------------------**/
        $(document).on('click','#forum_search',function(){
            $('.forum-search-popup-area').toggle();
        });

        /**--------------------------
         *  Jquery Dropipy Uploader
         * ------------------------ */
        // plugin url
        //https://github.com/JeremyFagis/dropify
        
        var dropzone = $('.dropify');
        if(dropzone.length > 0){
            dropzone.dropify();
        } 
        
        /**-------------------------
         *  Jquery Star Rating 
         * -----------------------**/

        //plugin github repository 
        //https://github.com/dobtco/starrr
        if($('.starrr').length > 0){
            $('.starrr').starrr({
                rating: 4,
                max: 5,
                //readOnly: true
                change: function(e, value){

                    $(this).next('input').val(value)
                    // getrating

                    //on change event for ajax call or manage rating from backend
                }
            })
        }
        
        /*--------------------
            wow js init
        ---------------------*/
        new WOW().init();

        /*-------------------------
            magnific popup activation
        -------------------------*/
        $('.video-play-btn,.video-popup,.small-vide-play-btn').magnificPopup({
            type: 'video'
        });
        /*------------------
            back to top
        ------------------*/
        $(document).on('click', '.back-to-top', function () {
            $("html,body").animate({
                scrollTop: 0
            }, 2000);
        });
        /*-------------------------
            counter section activation
        ---------------------------*/
        var counternumber = $('.count-num');
        counternumber.counterUp({
            delay: 20,
            time: 1500 
        });
        /*--------------------------------
        Testimonial Slick Carousel .testimonial-text-slider .testimonial-image-slider
        -----------------------------------*/
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            asNavFor: '.slider-nav'
        });
        /*------------------------------------
        Testimonial Slick Carousel as Nav
        --------------------------------------*/
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
            centerPadding: '10px',
            autoplay: true,
            responsive: [
                {
                breakpoint: 450,
                settings: {
                    dots: false,
                    slidesToShow: 3,  
                    centerPadding: '0px',
                    }
                },
                {
                breakpoint: 420,
                settings: {
                    autoplay: true,
                    dots: false,
                    slidesToShow: 1,
                    centerMode: false,
                    }
                }
            ]
        });
        /*---------------------------
            Logo carousel
        ---------------------------*/
        var $logoCarousel = $('#brand-carousel');
        if ($logoCarousel.length > 0) {
            $logoCarousel.owlCarousel({
                loop: true,
                autoplay: true, //true if you want enable autoplay
                autoPlayTimeout: 1000,
                margin: 30,
                dots: false,
                nav: false,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                smartSpeed:2000,
                responsive: {
                    0: {
                        items: 1
                    },
                    460: {
                        items: 2
                    },
                    599: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    960: {
                        items: 3
                    },
                    1200: {
                        items: 5
                    },
                    1920: {
                        items: 5
                    }
                }
            });
        }
        
        /*---------------------------
            Involved carousel
        ---------------------------*/
        var $InvolvedCarousel = $('#involved-carousel');
        if ($InvolvedCarousel.length > 0) {
            $InvolvedCarousel.owlCarousel({
                loop: true,
                autoplay: true, //true if you want enable autoplay
                autoPlayTimeout: 1000,
                margin: 30,
                dots: false,
                nav: false,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                smartSpeed:2000,
                responsive: {
                    0: {
                        items: 1
                    },
                    460: {
                        items: 2
                    },
                    599: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    960: {
                        items: 3
                    },
                    1200: {
                        items: 5
                    },
                    1920: {
                        items: 5
                    }
                }
            });
        }
    /*********************************************
     *  We can develop area animatin effect
    **********************************************/
    var nodes = document.querySelectorAll('.wecan-develop-list li');
    var curIndex = 0;
    
    setInterval(function() {
        var element = nodes[curIndex];
        $('.wecan-develop-list li').removeClass('active');
        $(element).addClass('active');
        if (curIndex == 5) {
            curIndex = 0;
        } else {
            curIndex++;
        }
    }, 1000);
       /******************************************** 
         *  Featured items move hove element
        * *****************************************/
       if ( winwidth > 768 ){

        $(document).on('mouseover','.single-latest-item',function(){
            var imgsrc = $(this).data('imgurl');
            var imgalt = $(this).data('imgalt');
            var title = $(this).data('title');
            $(this).append('<div class="hover-effect"><div class="hover-part-img"><img src="'+imgsrc+'" alt="'+imgalt+'"></div><div class="hover-text"><h3>'+title+'</h3></div></div>');
        });
        $(document).on('mouseout','.single-latest-item',function(){
            $(this).children('.hover-effect').remove();
        });
        //== Smooth scroll ==

        $(document).on('click','.navbar-area .navbar-nav li a',function (e) {
            // e.preventDefault();
            var anchor = $(this).attr('href');
            var hash = anchor.slice(0,1);
            if( '#' == hash ){
                e.preventDefault();
                var top = $(anchor).offset().top;
                $('html, body').animate({
                    scrollTop: $(anchor).offset().top
                }, 1000);
            }
        });
    }

    });
    //define variable for store last scrolltop
    var lastScrollTop = '';
    $(window).on('scroll', function () {
        //back to top show/hide
       var ScrollTop = $('.back-to-top');
       if ($(window).scrollTop() > 1000) {
           ScrollTop.fadeIn(1000);
       } else {
           ScrollTop.fadeOut(1000);
       }
       /*--------------------------
        sticky menu activation
       -------------------------*/
        var st = $(this).scrollTop();
        var mainMenuTop = $('.navbar-area');
        if ($(window).scrollTop() > 1000) {
            if (st > lastScrollTop) {
                // hide sticky menu on scrolldown 
                mainMenuTop.removeClass('nav-fixed');
                
            } else {
                // active sticky menu on scrollup 
                mainMenuTop.addClass('nav-fixed');
            }

        } else {
            mainMenuTop.removeClass('nav-fixed ');
        }
        lastScrollTop = st;
       
    });

    $(window).on('resize',function(){
        var winwidth = $(window).width();

        if(winwidth > 768 && winwidth <= 991 ){
            $('.g-recaptcha').css({
                transform:'scale(2.090)',
                clear : 'both',
                'margin-bottom': '100px'
            });

        } else if(winwidth > 599 && winwidth < 768){
            $('.g-recaptcha').css({
                transform:'scale(1.49)',
                clear : 'both',
                'margin-bottom': '50px'
            });
        }
         else if(winwidth > 451 && winwidth < 599){
            $('.g-recaptcha').css({
                transform:'scale(1.22)',
                clear : 'both',
                'margin-bottom': '20px'
            });
        } else if(winwidth > 415 && winwidth < 450){
            $('.g-recaptcha').css({
                transform:'scale(1.09)',
                clear : 'both',
                'margin-bottom': '20px'
            });
        }
        else if(winwidth > 384 && winwidth < 414){
            $('.g-recaptcha').css({
                transform:'scale(1)',
                clear : 'both',
                'margin-bottom': '10px'
            });
        }else if(winwidth > 360 && winwidth < 384){
            $('.g-recaptcha').css({
                transform:'scale(0.9)',
                clear : 'both',
                'margin-bottom': '0px'
            });
        }
        
    });
           
    $(window).on('load',function(){
        /*-----------------
            preloader
        ------------------*/
        var preLoder = $("#preloader");
        preLoder.fadeOut(1000);
        /*-----------------
            back to top
        ------------------*/
        var backtoTop = $('.back-to-top')
        backtoTop.fadeOut(100);
    });

}(jQuery));	
