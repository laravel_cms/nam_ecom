@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    <div class="header-area header-bg">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <div class="header-inner "><!-- header inner -->
                    <div class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><span class="subtitle">Team of Creative</span></div>
                    <h1 class="title wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">Designers &amp; Developers</h1>
                </div><!-- //. header inner -->
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 remove-col-padding">
                    <div class="header-bottom-inner">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="left-content-area">
                                    <h2 class="title">We Develop Digital Strategies, Products and Services.</h2>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-content-area">
                                    <div class="right-content-inner">
                                        <span class="separator"></span>
                                        <p>Creating fusion of visual design and technology to make awesome products that feel great on every desktop and mobile device also perform efficiently with minimum resource utilization and maximum performance.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="we-can-develop  margin-top-180   ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title"><!-- section title -->
                    <h2 class="title">We Can Develop Your Dream</h2>
                    <p>Architecting secure, efficient and user-friendly applications and systems by writing standard, well-documented and efficient codes to turn ideas into reality.</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="left-content"><!-- left content -->
                    <ul class="wecan-develop-list">
                        <li class="">Unique Innovations</li>
                        <li class="">Premium Quality</li>
                        <li class="">Industry Standards</li>
                    </ul>
                </div><!-- //. left content -->
            </div>
            <div class="col-lg-6">
                <div class="middle-content"><!-- middle content -->
                    <div class="img-wrapper">
                        <img src="https://thesoftking.com/assets/images/we-can-develop-img.jpg" alt="we can develop image">
                        <div class="hover">
                            <a href="https://www.youtube.com/watch?v=GT6-H4BRyqQ" class="video-play-btn mfp-iframe"> <i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div><!-- //. middle content -->
            </div>
            <div class="col-lg-3">
                <div class="right-content"><!-- right content -->
                    <ul class="wecan-develop-list">
                        <li class="">250+ Unique Items</li>
                        <li class="">After Sales support</li>
                        <li class="active">Custom Development</li>
                    </ul>
                </div><!-- //. right content -->
                 <ul class="wecan-develop-list-mobile-version">
                    <li>Unique Innovations</li>
                    <li>Premium Quality</li>
                    <li>Industry Standards</li>
                    <li>250+ Unique Items</li>
                    <li>After Sales support</li>
                    <li>Custom Development</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="featured-item-area featured-item-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title white"><!-- section title -->
                    <h2 class="title">Featured Items</h2>
                    <p>Items with current trend, special feature and something unique are here. In one word our best items are listed here. </p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row">
                            <div class="col-lg-4 col-md-6">
                    <div class="single-featured-item"><!--- single feature item -->
                        <div class="thumb">
                            <img src="https://thesoftking.com/assets/product/upload/cover/5c375c537f7e3.jpg" alt="PonziPRO - P2P Donation Platform">
                        </div>
                        <div class="content">
                            <a href="https://thesoftking.com/item/ponzipro-p2p-donation-platform/273"><h4 class="title"> PonziPRO - P2P Donation Platform</h4></a>
                            <div class="bottom-content">
                                <div class="left-content">
                                    <ul class="review">
                                                                                                                                                                    <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                            </ul>
                                </div>
                                <div class="right-content">
                                    <span class="price">$199.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="col-lg-4 col-md-6">
                    <div class="single-featured-item"><!--- single feature item -->
                        <div class="thumb">
                            <img src="https://thesoftking.com/assets/product/upload/cover/5c375a5936479.jpg" alt="DogeCash - DogeCoin Cloud Mining Platform">
                        </div>
                        <div class="content">
                            <a href="https://thesoftking.com/item/dogecash-dogecoin-cloud-mining-platform/271"><h4 class="title"> DogeCash - DogeCoin Cloud Mining Platform</h4></a>
                            <div class="bottom-content">
                                <div class="left-content">
                                    <ul class="review">
                                                                                                                                                                    <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                            </ul>
                                </div>
                                <div class="right-content">
                                    <span class="price">$199.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <div class="col-lg-4 col-md-6">
                    <div class="single-featured-item"><!--- single feature item -->
                        <div class="thumb">
                            <img src="https://thesoftking.com/assets/product/upload/cover/5c3759612228b.jpg" alt="CoinTrade - Localbitcoin Clone P2P Exchanger">
                        </div>
                        <div class="content">
                            <a href="https://thesoftking.com/item/cointrade-localbitcoin-clone-p2p-exchanger/270"><h4 class="title"> CoinTrade - Localbitcoin Clone P2P Exchang...</h4></a>
                            <div class="bottom-content">
                                <div class="left-content">
                                    <ul class="review">
                                                                                                                                                                    <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                                            <li><i class="fas fa-star"></i></li>
                                                                                                                                                            </ul>
                                </div>
                                <div class="right-content">
                                    <span class="price">$199.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="btn-wrapper margin-top-60">
                    <a href="https://thesoftking.com/products/featured" class="btn btn-tsk-outline color-white ">More From Our Store</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="latest-item-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title"><!-- section title -->
                    <h2 class="title">Latest Products</h2>
                    <p>We are working on a daily basis to bring new, unique, standard and trendy product. Check out the newest products from our awesome team.</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row latest-item-row">
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/hoster-domain-hosting-business-html-templates/275">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c422275c4b5a.jpg" data-imgalt="Hoster – Domain Hosting Business HTML Templates" data-title="Hoster – Domain Hosting Business HTML Templates"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c422276596bbicon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Hoster</h4>
                                <div class="price-area">
                                    <span class="sales">0 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/movion-movies-info-html-template/268">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c36e0f1749ae.jpg" data-imgalt="Movion - Movies Info HTML Template" data-title="Movion - Movies Info HTML Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c36e0f1c54e1icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Movion</h4>
                                <div class="price-area">
                                    <span class="sales">0 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/comtech-computer-repair-shop-business-html-template/267">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c358e1d8f012.jpg" data-imgalt="ComTech - Computer &amp; Repair Shop Business HTML Template" data-title="ComTech - Computer &amp; Repair Shop Business HTML Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c358e1de5e37icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">ComTech</h4>
                                <div class="price-area">
                                    <span class="sales">2 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/yonza-yoga-html-template/266">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c358e0ca973f.jpg" data-imgalt="Yonza - Yoga HTML Template" data-title="Yonza - Yoga HTML Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c358e0d3e34ficon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Yonza</h4>
                                <div class="price-area">
                                    <span class="sales">0 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/marafi-digital-marketing-agency-psd-template/265">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c34ca66d1393.jpg" data-imgalt="Marafi - Digital Marketing Agency PSD Template" data-title="Marafi - Digital Marketing Agency PSD Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c34ca6754b60icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Marafi</h4>
                                <div class="price-area">
                                    <span class="sales">1 Sales</span>
                                    <span class="price">$12.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/mediflex-medical-doctor-health-care-html-template/264">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c30475c89cd3.jpg" data-imgalt="Mediflex - Medical Doctor &amp; Health Care HTML Template" data-title="Mediflex - Medical Doctor &amp; Health Care HTML Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c30475cecee0icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Mediflex</h4>
                                <div class="price-area">
                                    <span class="sales">1 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/musier-music-html-template/263">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c2dc94c11803.jpg" data-imgalt="Musier - Music HTML Template" data-title="Musier - Music HTML Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c2dc94c72323icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">Musier</h4>
                                <div class="price-area">
                                    <span class="sales">2 Sales</span>
                                    <span class="price">$17.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/mixhost-web-hosting-psd-template/262">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c29031beab82.jpg" data-imgalt="MixHost - Web Hosting PSD Template" data-title="MixHost - Web Hosting PSD Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c29031ca55a3icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">MixHost</h4>
                                <div class="price-area">
                                    <span class="sales">1 Sales</span>
                                    <span class="price">$12.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/barbaraz-hair-saloon-beauty-parlour-psd-template/261">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c1e059e46fb3.jpg" data-imgalt="BarBaRaz - Hair Saloon &amp; Beauty Parlour PSD Template" data-title="BarBaRaz - Hair Saloon &amp; Beauty Parlour PSD Template"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c1e059eac149icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">BarBaRaz</h4>
                                <div class="price-area">
                                    <span class="sales">0 Sales</span>
                                    <span class="price">$12.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/dizcuz-forum-discussion-community-network/260">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c1b397d9a5aa.jpg" data-imgalt="DizCuz - Forum Discussion Community Network" data-title="DizCuz - Forum Discussion Community Network"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c1b397dcf0d9icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">DizCuz</h4>
                                <div class="price-area">
                                    <span class="sales">2 Sales</span>
                                    <span class="price">$39.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/telesign-crypto-signal-telegram-bot/259">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c1b397772097.jpg" data-imgalt="TeleSign - Crypto Signal Telegram Bot" data-title="TeleSign - Crypto Signal Telegram Bot"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c1b3977d75f9icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">TeleSign</h4>
                                <div class="price-area">
                                    <span class="sales">6 Sales</span>
                                    <span class="price">$39.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                            <div class="col-xs-6 col-sm-4 col-lg-2 col-md-3 latest-item-col">
                    <a href="https://thesoftking.com/item/tramate-complete-travel-business-solution/258">
                        <div class="single-latest-item" data-imgurl="https://thesoftking.com/assets/product/upload/cover/5c19d9463a6cf.jpg" data-imgalt="TraMate - Complete Travel Business Solution" data-title="TraMate - Complete Travel Business Solution"><!--- single latest item -->
                            <div class="thumb">
                                <img src="https://thesoftking.com/assets/product/upload/cover/5c19d946828a5icon.jpg" alt="latest image">
                            </div>
                            <div class="content">
                                <h4 class="title">TraMate</h4>
                                <div class="price-area">
                                    <span class="sales">7 Sales</span>
                                    <span class="price">$69.00</span>
                                </div>
                            </div>
                        </div><!-- //. single latest item -->
                    </a>
                </div>
                    </div>
        <div class="row">
            <div class="col-lg-12 text-center">

                <div class="forum-pagination margin-top-40">
                    <div class="btn-wrapper margin-top-30">
                        <a href="https://thesoftking.com/products" class="btn btn-tsk-outline ">More From Our Store</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="counterup-area counterup-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-counterup-item"><!-- single counter up item -->
                    <div class="top-content">
                        <div class="icon">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="count"><span class="count-num">7</span>+</div>
                    </div>
                    <h4 class="title">Years Online</h4>
                </div><!-- //.single counter up item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-counterup-item"><!-- single counter up item -->
                    <div class="top-content">
                        <div class="icon">
                            <i class="fas fa-code"></i>
                        </div>
                        <div class="count"><span class="count-num">250</span>+</div>
                    </div>
                    <h4 class="title">Unique Items</h4>
                </div><!-- //.single counter up item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-counterup-item"><!-- single counter up item -->
                    <div class="top-content">
                        <div class="icon">
                            <i class="fas fa-cart-plus"></i>
                        </div>
                        <div class="count"><span class="count-num">5800</span>+</div>
                    </div>
                    <h4 class="title">Item Sold</h4>
                </div><!-- //.single counter up item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-counterup-item"><!-- single counter up item -->
                    <div class="top-content">
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="count"><span class="count-num">3800</span>+</div>
                    </div>
                    <h4 class="title">Happy Clients</h4>
                </div><!-- //.single counter up item -->
            </div>
        </div>
    </div>
</section>

<section class="supported-by-human-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title"><!-- section title -->
                    <h2 class="title">Supported By Humans</h2>
                    <p>We put special emphasis on customer support. Our dedicated support team is waiting to assist you. We always try our level best to do so.</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-supported-by-human-item"><!-- single supported by human item -->
                    <span class="count-num">4</span>
                    <h5 class="title">Dedicated Support Staff</h5>
                </div><!-- //. single supported by human item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-supported-by-human-item"><!-- single supported by human item -->
                    <span class="count-num">6</span>
                    <h5 class="title">Days in a Week</h5>
                </div><!-- //. single supported by human item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-supported-by-human-item"><!-- single supported by human item -->
                    <span class="count-num">10</span>
                    <h5 class="title">Hours in a Day</h5>
                </div><!-- //. single supported by human item -->
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-supported-by-human-item"><!-- single supported by human item -->
                    <div class="count"> <span class="count-num">120</span>+</div>
                    <h5 class="title">Avg. Daily Ticket Issued</h5>
                </div><!-- //. single supported by human item -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="single-supported-item-two margin-top-30"><!-- single supported item two -->
                    <div class="icon">
                        <i class="far fa-lightbulb"></i>
                    </div>
                    <div class="content">
                        <h4 class="title">Any Custom Idea?</h4>
                        <p>Any awesome idea  needs to turn into reality? or any customization needed?  </p>
                        <div class="btn-wrapper">
                            <a href="https://thesoftking.com/contact" class="btn-tsk-outline">set a Metting</a>
                        </div>
                    </div>
                </div><!-- //.single supported item two -->
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-supported-item-two margin-top-30"><!-- single supported item two -->
                    <div class="icon">
                        <i class="far fa-life-ring"></i>
                    </div>
                    <div class="content">
                        <h4 class="title">Facing Technical Issue?</h4>
                        <p>Facing any technical dificulties with our products? Relax! We are here to assist you. </p>
                        <div class="btn-wrapper">
                            <a href="https://portal.thesoftking.com/submitticket.php?step=2&amp;deptid=2" target="_blank" class="btn-tsk-outline">get support</a>
                        </div>
                    </div>
                </div><!-- //.single supported item two -->
            </div>
        </div>
    </div>
</section>

<section class="testimonial-area testimonial-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title white"><!-- section title -->
                    <h2 class="title">What People Say</h2>
                    <p>We care about our clients and they do same for us. Have a look below and see what our awesome clients says about their experiance with us!</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="testimonial-text-slider slider-for text-center slick-initialized slick-slider">

                    <div aria-live="polite" class="slick-list"><div class="slick-track" style="opacity: 1; width: 7920px; transform: translate3d(-1440px, 0px, 0px);" role="listbox"><div class="sin-testiText slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 720px;">
                        <p>Everyone who but this website please be appreciative by giving this guys 5 stars.Very well design clean code and Fast, accurate support service.I must give you more than 5 star so you have 100 stars from me, well done guys keep up with your good work and very well recommended !!!</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 720px;">
                        <p>I just want to thank the support staff because they have been wonderful! I have never been left alone and despite my repeated requests, they have assured.Thank you </p>
                    </div><div class="sin-testiText slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 720px;">
                        <p>Good template to start with. I had one minor issue with implementation but was resolved with fast reply from Thesoftking. Will use Thesoftking for some aditional costumisation in the future - since it will be done the fastest with their help.</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 720px;">
                        <p>It's a good script. Installation easy in two minutes. It's very easy to configure. The support is perfect, the support help you in the installation or any questions you have very fast and correct. It's 100% recommended.</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 720px;">
                        <p>Seller did an amazing job helping me out with everything. Even changed some coding for me, free of charge which was great. Supplied some free hosting also. This seller is A++++</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="4" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide04" style="width: 720px;">
                        <p>customer service is very excellent and I love your software. I give it a 100% for your e-wallet, I have to give some one credit when they deserve it and you have earned it, wish you all the best with a lot of sales and success.</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="5" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide05" style="width: 720px;">
                        <p>Its product is original and good, installs in 5 minutes, works great and is very well written code. Speed optimizations will already come minimizing, but the code is clean and clear, so that everyone can adjust it to their needs. I'm going to give you my five points and also thank you. I wish you lots of sales, I will be attentive to your new products.</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide06" style="width: 720px;">
                        <p>Awesome Features. Fantastic user interface. Everything is running very nicely on my website. I just purchased today and got some issues as i m non technical person but Thesoftking provide me step to step solutions.</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="7" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide07" style="width: 720px;">
                        <p>Very nice design quality 5 stars cause the designer should be the best! Thanks ThemeForest to bring me that diamond work! The highest best coded and designed html files i have even seen! Purchase it and you won't regret it! Very low prices for the work that's made!</p>
                    </div><div class="sin-testiText slick-slide" data-slick-index="8" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide08" style="width: 720px;">
                        <p>Everyone who but this website please be appreciative by giving this guys 5 stars.Very well design clean code and Fast, accurate support service.I must give you more than 5 star so you have 100 stars from me, well done guys keep up with your good work and very well recommended !!!</p>
                    </div><div class="sin-testiText slick-slide slick-cloned" data-slick-index="9" aria-hidden="true" tabindex="-1" style="width: 720px;">
                        <p>I just want to thank the support staff because they have been wonderful! I have never been left alone and despite my repeated requests, they have assured.Thank you </p>
                    </div></div></div>

                    

                    

                    


                    

                    

                    

                    

                    

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-7">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="testimonial-image-slider slider-nav text-center slick-initialized slick-slider">

                            <div aria-live="polite" class="slick-list draggable" style="padding: 0px 10px;"><div class="slick-track" role="listbox" style="opacity: 1; width: 3434px; transform: translate3d(-808px, 0px, 0px);"><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/03.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Lealmar</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="-3" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/04.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>sanjeetuts</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/01.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4> kratos-myth</h4>
                                        <h5>Buyer, Themeforrest</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/02.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>robertotamere</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide10" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/01.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Staricson</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-current slick-active slick-center" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide11" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/02.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Pepsy_si</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide12" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/03.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>javicerillos</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide13" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/04.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Letsgetonline</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="4" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide14" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/02.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>sabba24</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="5" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide15" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/03.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Lealmar</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="6" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide16" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/04.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>sanjeetuts</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="7" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide17" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/01.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4> kratos-myth</h4>
                                        <h5>Buyer, Themeforrest</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide" data-slick-index="8" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide18" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/02.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>robertotamere</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="9" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/01.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Staricson</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="10" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/02.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Pepsy_si</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="11" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/03.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>javicerillos</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div><div class="sin-testiImage slick-slide slick-cloned" data-slick-index="12" aria-hidden="true" tabindex="-1" style="width: 202px;">
                                <div class="user-info">
                                    <div class="part-img">
                                        <img src="https://thesoftking.com/assets/images/testimonial/04.jpg" alt="testimonial image">
                                    </div>
                                    <div class="part-user">
                                        <h4>Letsgetonline</h4>
                                        <h5>Buyer, Codecanyon</h5>
                                    </div>
                                </div>
                            </div></div></div>

                            

                            

                            


                            

                            

                            

                            

                            



                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</section>

<section class="latest-annoucement-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <div class="section-title"><!-- section title -->
                    <h2 class="title">Latest Announcements</h2>
                    <p>Our offers, innovations and latest updates of our team is posted here. Read our latest announcements to know what we are doing.</p>
                </div><!-- //. section title -->
            </div>
        </div>
        <div class="row">
                        <div class="col-lg-4 col-md-6">
                <div class="single-annoucement-item"><!-- single announcement item -->
                    <div class="thumb">
                        <img src="https://thesoftking.com/assets/images/1547105716.jpg" alt="We Upgrade Our LiveChat, Sales &amp; Technical Support Team">
                        <div class="hover">
                            <ul class="post-meta">
                                <li>Jan</li>
                                <li>10</li>
                                <li>2019</li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <a href="https://thesoftking.com/announcement/we-upgrade-our-livechat-sales-technical-support-team/5"><h4 class="title">We Upgrade Our LiveChat, Sales &amp; Technical Support Team</h4></a>
                        <p>
                            Dear customers,As part of our commitment to provide you with better and more efficient 24/7 online support services, we are pleased to inform you that we are upgrading our Livechat system. Our new system will help us to offer more features and functionality in your support experience.Our Livechat Ag...
                        </p>
                        <a href="https://thesoftking.com/announcement/we-upgrade-our-livechat-sales-technical-support-team/5" class="readmore">Read more</a>
                    </div>
                </div><!-- //. single announcement item -->
            </div>
                            <div class="col-lg-4 col-md-6">
                <div class="single-annoucement-item"><!-- single announcement item -->
                    <div class="thumb">
                        <img src="https://thesoftking.com/assets/images/1542900548.jpg" alt="New Website Design &amp; Client Portal Launch With Exciting Features">
                        <div class="hover">
                            <ul class="post-meta">
                                <li>Nov</li>
                                <li>22</li>
                                <li>2018</li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <a href="https://thesoftking.com/announcement/new-website-design-client-portal-launch-with-exciting-features/4"><h4 class="title">New Website Design &amp; Client Portal Launch With Exciting Features</h4></a>
                        <p>
                            We are very excited to have launched our new website. The redesign has been in progress for almost two months and that time was well spent. We have been working extremely hard over the past several months to create an entirely new client control panel as well as our website which comes standard with...
                        </p>
                        <a href="https://thesoftking.com/announcement/new-website-design-client-portal-launch-with-exciting-features/4" class="readmore">Read more</a>
                    </div>
                </div><!-- //. single announcement item -->
            </div>
                            <div class="col-lg-4 col-md-6">
                <div class="single-annoucement-item"><!-- single announcement item -->
                    <div class="thumb">
                        <img src="https://thesoftking.com/assets/images/1542899177.jpg" alt="How to Choose A Best Web Hosting Company For Your Website">
                        <div class="hover">
                            <ul class="post-meta">
                                <li>Nov</li>
                                <li>22</li>
                                <li>2018</li>
                            </ul>
                        </div>
                    </div>
                    <div class="content">
                        <a href="https://thesoftking.com/announcement/how-to-choose-a-best-web-hosting-company-for-your-website/3"><h4 class="title">How to Choose A Best Web Hosting Company For Your Website</h4></a>
                        <p>
                            You want your business to succeed, so do not frustrate your customers and risk losing their business by hosting your website with an unreliable company. Make sure you take the time to research everything about the company and find out if it will be a fit for your needs.&amp;nbsp;1. Make sure you researc...
                        </p>
                        <a href="https://thesoftking.com/announcement/how-to-choose-a-best-web-hosting-company-for-your-website/3" class="readmore">Read more</a>
                    </div>
                </div><!-- //. single announcement item -->
            </div>
                        </div>
    </div>
</section>



@endsection