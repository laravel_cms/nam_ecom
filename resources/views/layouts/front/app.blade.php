<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('GOOGLE_ANALYTICS') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ env('GOOGLE_ANALYTICS') }}');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    <title>Laracom - Laravel FREE E-Commerce Software</title>
    <meta name="description" content="Modern open-source e-commerce framework for free">
    <meta name="tags" content="modern, opensource, open-source, e-commerce, framework, free, laravel, php, php7, symfony, shop, shopping, responsive, fast, software, blade, cart, test driven, adminlte, storefront">
    <meta name="author" content="Jeff Simons Decena">


    <link rel="stylesheet" href="{{ asset('king_theme/css/bootstrap.min.css') }}">
    <!-- icofont -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/fontawesome.min.css') }}">
    <!-- animate.css -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/animate.css') }}">
    <!-- slick.css -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('king_theme/css/dropify.min.css') }}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/owl.carousel.min.css') }}">
    <!-- magnific popup -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('king_theme/css/toastr.min.css') }}">
    <!-- stylesheet -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/style.css') }}">
        <!-- responsive -->
    <link rel="stylesheet" href="{{ asset('king_theme/css/responsive.css') }}">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
    <script src="{{ asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
    <![endif]-->
   
    <link rel="manifest" href="{{ asset('favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    @yield('css')
    <meta property="og:url" content="{{ request()->url() }}"/>
    @yield('og')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}"></script>
</head>
<body>
@include('layouts.front.header')

@yield('content')

@include('layouts.front.footer')


<!-- jquery -->
<script src="{{asset('king_theme/js/jquery.js') }}"></script>
<!-- popper -->
<script src="{{asset('king_theme/js/popper.min.js') }}"></script>
<!-- bootstrap -->
<script src="{{asset('king_theme/js/bootstrap.min.js') }}"></script>
<!-- way poin js-->
<script src="{{asset('king_theme/js/waypoints.min.js') }}"></script>
<!-- owl carousel -->
<script src="{{asset('king_theme/js/owl.carousel.min.js') }}"></script>
<!-- magnific popup -->
<script src="{{asset('king_theme/js/jquery.magnific-popup.js') }}"></script>
<!-- slick js -->
<script src="{{asset('king_theme/js/slick.min.js') }}"></script>
<script src="{{asset('king_theme/js/dropify.min.js') }}"></script>
<script src="{{asset('king_theme/js/starrr.js') }}"></script>
<!-- wow js-->
<script src="{{asset('king_theme/js/wow.min.js') }}"></script>
<!-- counterup js-->
<script src="{{asset('king_theme/js/jquery.counterup.min.js') }}"></script>

<script src="{{asset('king_theme/js/toastr.min.js') }}"></script>

<!-- main -->
<script src="{{asset('king_theme/js/main.js') }}"></script>
    <script>
        function formsubmit() {
            document.getElementById("searchform").submit();
        }

        $(document).ready(function () {
            $(document).on('click', '.sbtnFavourite', function () {
                var id = $(this).data('productid');
                var el = $(this);

                $.ajax({
                    type: "post",
                    url: "https://thesoftking.com/user/product/favourite",
                    data: {
                        id: id,
                        _token: "ggRLq25UhyR6iad2YchsXwB4P0SmAQE2vof16q6Y"
                    },
                    success: function (data) {
                        if (data.status == 0) {
                            el.children().css({"color": ""});
                        }
                        else if (data.status == 1) {
                            el.children().css({"color": "red"});
                        }
                    }
                    ,
                    error: function (res) {
                        $("#FavoriteFail").modal() 
                    }
                });
            });
        });


    </script>
<script>
            </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=" {{asset('king_theme/js_1') }}" ></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-129805464-1');
    </script>

    
    <!-- Start Alexa Certify Javasript -->
    <script>
    _atrk_opts = { atrk_acct:"Xnrkj1aAkN0007", domain:"thesoftking.com",dynamic: true};
    (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
    </script>
    <noscript><img src="images/atrk.gif" class="d-none" height="1" width="1" alt="*"></noscript>
    <!-- End Alexa Certify Javascript -->  
    
    <!--Start of Tawk.to Script-->
    <script>
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5c025998fd65052a5c934ef7/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

@yield('js')
</body>
</html>