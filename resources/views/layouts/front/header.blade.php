<!-- support bar area start -->
<div class="support-bar bg_dark">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="left-content-area"><!-- left content area -->
                    <ul>
                        <li><a href="https://thesoftking.com/contact"><i class="fas fa-envelope"></i> software@thesoftking.com</a></li>
                        <li><a href="skype:rifat636"><i class="fab fa-skype"></i> rifat636</a></li>
                    </ul>
                </div><!-- //. left content aera -->
                <div class="right-content-area"><!-- right content area -->
                    <ul>
                                                    <li><a href="https://thesoftking.com/login"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            <li><a href="https://thesoftking.com/register"><i class="fas fa-user-plus"></i> Register</a></li>
                                            </ul>
                </div><!-- //.right content area -->
            </div>
        </div>
    </div>
</div>
<!-- support bar area end -->
<!-- navbar area start -->
<nav class="navbar navbar-area  not-absolute navbar-expand-lg navbar-light margin-top-50">
    <div class="container nav-container">
        <div class="logo-wrapper navbar-brand">
            <a href="https://thesoftking.com" class="logo main-logo">
                <img src="{{ asset('king_theme/images/logo.png') }}" alt="logo">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="mirex">
            <!-- navbar collapse start -->
            <ul class="navbar-nav">
                <!-- navbar- nav -->
                <li class="nav-item">
                    <a class="nav-link pl-0" href="https://thesoftking.com">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">About</a>
                    <div class="dropdown-menu">
                        <a href="https://thesoftking.com/about" class="dropdown-item">Company Intro</a>
                        <a href="https://thesoftking.com/giving-back" class="dropdown-item">Giving Back</a>
                        <a href="https://thesoftking.com/timeline" class="dropdown-item">Our Timeline</a>
                    </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="https://thesoftking.com/services">Services</a></li>

                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Products</a>
                    <div class="dropdown-menu">
                                                <a href="https://thesoftking.com/products/php-script" class="dropdown-item">PHP Script</a>
                                                    <a href="https://thesoftking.com/products/html-template" class="dropdown-item">HTML Template</a>
                                                    <a href="https://thesoftking.com/products/psd-template" class="dropdown-item">PSD Template</a>
                                                    <a href="https://thesoftking.com/products/plugin-extenshion" class="dropdown-item">Plugin &amp; Extenshion</a>
                                                </div>
                </li>
                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Web Hosting</a>
                    <div class="dropdown-menu">
                        <a href="https://thesoftking.com/hosting/shared" class="dropdown-item">Shared Hosting</a>
                        <a href="https://thesoftking.com/hosting/vps" class="dropdown-item"> VPS Hosting</a>
                        <a href="https://thesoftking.com/hosting/dedicated" class="dropdown-item">Dedicated Server</a>
                        <a href="https://thesoftking.com/hosting/shoutcast" class="dropdown-item">Radio Shoutcast</a>
                        <a href="https://thesoftking.com/data-center" class="dropdown-item">Data Center</a>
                    </div>
                </li>

                <li class="nav-item"><a class="nav-link" href="https://thesoftking.com/forum">Forum</a></li>

                <li class="nav-item">
                    <a class="nav-link" href="https://thesoftking.com/contact">Contact</a>
                </li>
                <li class="nav-item nav-btn">
                    <a class="nav-link" href="https://thesoftking.com/hire-us">Hire Us</a>
                </li>
            </ul>
            <!-- /.navbar-nav -->
        </div>
        <!-- /.navbar btn wrapper -->
        <div class="responsive-mobile-menu">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mirex" aria-controls="mirex" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <!-- navbar collapse end -->
    </div>
</nav>
<!-- navbar area end -->
