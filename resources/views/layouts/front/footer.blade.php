

<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="col-xl-12 col-lg-12 px-0">
                <div class="brand-carousel" id="brand-carousel">
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/01.png') }}  " alt="brangs logo">
                    </div>
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/02.png') }}" alt="brangs logo">
                    </div>
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/03.png') }}" alt="brangs logo">
                    </div>
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/04.png') }}" alt="brangs logo">
                    </div>
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/05.png') }}" alt="brangs logo">
                    </div>
                    <div class="single-brand-logo">
                        <img src="{{ asset('king_theme/images/06.png') }}" alt="brangs logo">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="part-about">
                        <a href="https://thesoftking.com"><img src="{{ asset('king_theme/images/footer-logo.png') }}" alt="thesoftking"></a>
                        <p>Founded At 2011. Architecting secure, efficient and user-friendly items by writing codes to turn ideas into reality. Working on a daily basis to bring unique, standard and trendy product for various marketplace and clients.</p>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-6">
                    <div class="part-link">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="https://thesoftking.com/about">About Us</a></li>
                            <li><a href="https://thesoftking.com/timeline">Our Timeline</a></li>
                            <li><a href="https://thesoftking.com/data-center">Datacenter</a></li>
                            <li><a href="https://thesoftking.com/services">Our Services</a></li>
                            <li><a href="https://thesoftking.com/career">Career with us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-6">
                    <div class="part-link">
                        <h3>Our Expertise</h3>
                        <ul>
                            <li><a href="https://thesoftking.com/services/web">Web Application</a></li>
                            <li><a href="https://thesoftking.com/services/apps">Mobile Apps</a></li>
                            <li><a href="https://thesoftking.com/services/ui-ux">UI/UX</a></li>
                            <li><a href="https://thesoftking.com/services/plugin-extension">Plugin & Extension</a></li>
                            <li><a href="https://thesoftking.com/services/consultancy">IT Consultancy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-6">
                    <div class="part-link">
                        <h3>Terms</h3>
                        <ul>
                            <li><a href="https://thesoftking.com/announcement">Announcements</a></li>
                            <li><a href="https://thesoftking.com/privacy">Privacy Policy</a></li>
                            <li><a href="https://thesoftking.com/terms">Terms of Service</a></li>
                            <li><a href="https://thesoftking.com/refund-policy">Refund Policy</a></li>
                            <li><a href="https://thesoftking.com/licences-info">Licences info</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-6">
                    <div class="part-link">
                        <h3>Support</h3>
                        <ul>
                            <li><a href="https://thesoftking.com/faq">F.A.Q.</a></li>
                            <li><a href="https://portal.thesoftking.com/submitticket.php?step=2&deptid=2">Get Support</a></li>
                            <li><a href="https://thesoftking.com/forum">Our Forum</a></li>
                            <li><a href="https://thesoftking.com/login">Member Area</a></li>
                            <li><a href="https://thesoftking.com/contact">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-7">
                    <div class="part-copyright">
                        <h3>© 2011 - 2019 THESOFTKING. All Rights Reserved.</h3>
                        <p>TheSoftKing is not partnered with any other company or person. We work as a team and do not have any reseller, distributor or partner!</p>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="part-social">
                        <h4>Stay Connected</h4>
                        <a href="https://www.facebook.com/thesoftking" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://twitter.com/thesoftking" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.linkedin.com/company/thesoftking" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="https://www.youtube.com/channel/UCG7KVjCMa5LLI9PxW6XW7Hw" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="back-to-top" class="back-to-top">
    <i class="fas fa-rocket"></i>
</div> 

<!-- preloader area start -->
 <div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="loader">
            TSK
            <span></span>
        </div>
    </div>
</div> 
<!-- preloader area end -->
